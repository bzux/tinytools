#!/bin/sh
# extracts audio from mp4 file and concatenate resulting audio into one wav file
# 
# ===========================================
# Variables
gppath='/home/bruno/Downloads/video'
extlength=4
dt=`date '+%Y-%m-%d_%H%M'`
# ===========================================

# change into working directory
cd $gppath

# extract mp3
for file in $gppath/*
do
	fname=$(basename "$file")
	sfname=${fname:0:$((${#fname} - $extlength))}
	ffmpeg -i $fname -f mp3 -ab 192000 -vn $sfname.mp3	
done

# create a list of resulting audio files
for f in ./*.mp3; do echo "file '$f'" >> input.txt; done

# concatenate audio files
ffmpeg -f concat -safe 0 -i input.txt -c copy $dt.wav

# cleanup
rm input.txt
rm *.mp3
