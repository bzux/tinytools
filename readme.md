# Tiny Tools
## goproaudio.sh
We have a new dog and I record him when he is alone using my GoPro. I'm mostly interested if and when he barks.  The script goproaudio.sh extracts audio from the mp4 file and concatenate resulting audio into one wav file. Using Audacity I can easily see when he barked.  

## updmcs.sh
Build the latest Spigot server, based on https://www.spigotmc.org/wiki/buildtools/
