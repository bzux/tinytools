#!/bin/sh
# Build the latest Spigot server
# Based on https://www.spigotmc.org/wiki/buildtools/
# ===========================================
# Variables
mcspath='/opt/mcs'
rev='latest'
# ===========================================

cd $mcspath
wget -O BuildTools.jar https://hub.spigotmc.org/jenkins/job/BuildTools/lastSuccessfulBuild/artifact/target/BuildTools.jar
java -jar BuildTools.jar --rev $rev

# cleanup
rm BuildTools.jar
